package crabster.rudakov.locationcombined

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.*
import com.google.android.material.snackbar.Snackbar
import crabster.rudakov.locationcombined.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var mainBinding: ActivityMainBinding
    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private val permissionId = 42

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mainBinding.root)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        mainBinding.btnLocation.setOnClickListener {
            getLastLocation()
        }
    }

    /**
     * Проверяем наличие разрешениий от пользователя на получение геолокации, в случае
     * их отсутствия - запрашиваем. Проверяем доступность получения геоданных, в случае
     * её отсутствия - просим пользователя включить отправку GPS сигнала. После устранения
     * всех возможных препятствий получаем последнюю зарегистрированную геопозицию
     **/
    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    val location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        getGeoCodingData(location)
                    }
                }
            } else {
                Snackbar
                    .make(mainBinding.root, "Please turn on location", Snackbar.LENGTH_INDEFINITE)
                    .show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    /**
     * Извлекаем геоданные с помощью геодекодирования, для чего передаём текущую
     * геопозицию в специальный объект Geocoder, с помощью которого создаётся список
     * необходимых данных. Далее устанавливаем значения в соответствующие View
     **/
    @SuppressLint("SetTextI18n")
    private fun getGeoCodingData(location: Location) {
        val geocoder = Geocoder(this, Locale.getDefault())
        val list: List<Address> =
            geocoder.getFromLocation(location.latitude, location.longitude, 1)
        mainBinding.apply {
            tvLatitude.text = "Latitude\n${list[0].latitude}"
            tvLongitude.text = "Longitude\n${list[0].longitude}"
            tvCountryName.text = "Country Name\n${list[0].countryName}"
            tvLocality.text = "Locality\n${list[0].locality}"
            tvAddress.text = "Address\n${list[0].getAddressLine(0)}"
        }
    }

    /**
     * Метод вызывается в случае отсутствия данных о предыдущей геолокации и получает
     * новую текущую геопозицию с высокой точностью
     **/
    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        val mLocationRequest = LocationRequest.create()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()!!
        )
    }

    /**
     * Коллбэк возвращает последнюю зафиксированную геолокацию
     **/
    private val mLocationCallback = object : LocationCallback() {
        @SuppressLint("SetTextI18n")
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location = locationResult.lastLocation
            getGeoCodingData(mLastLocation)
        }
    }

    /**
     * Проверяем доступна ли геолокация для получения(т.е. доступен ли какой-либо
     * из возможнных провайдеров)
     **/
    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    }

    /**
     * Проверяем были ли предоставлены необходимые разрешения(
     * ACCESS_COARSE_LOCATION - запрос доступа к данным о примерном местоположении
     * ACCESS_FINE_LOCATION - запрос доступа к данным о точном местоположении)
     **/
    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    /**
     * Запрашиваем разрешения на определение геолокации
     **/
    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            permissionId
        )
    }

    /**
     * Получаем решение пользователя, проверяя, что requestСode тот же, что мы
     * указывали в requestPermissions. В массиве permissions придут название разрешений,
     * которые мы запрашивали. В массиве grantResults придут ответы пользователя на
     * запросы разрешений. Мы проверяем, что массив ответов не пустой и берем первый
     * результат из него (т.к. мы запрашивали всего одно разрешение). Если пользователь
     * подтвердил разрешение, то выполняем операцию getLastLocation()
     **/
    @SuppressLint("MissingSuperCall")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == permissionId) {
            if ((grantResults.isNotEmpty() && grantResults[0]
                        == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
    }

}